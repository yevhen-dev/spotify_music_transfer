<!DOCTYPE html>
<html>
    <head>
        <title>Spotify music transfer</title>
        <link rel="stylesheet" type="text/css" href="assets/css/app.css">
        <script src="//vk.com/js/api/openapi.js?122" type="text/javascript"></script>
        <script src="assets/js/jquery-3.1.0.min.js" type="text/javascript"></script>
        <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="assets/js/jquery.mCustomScrollbar.concat.min.js" type="text/javascript"></script>
        <link rel="icon" type="image/png" href="assets/img/fav1.png">
    </head>
    <body>
        <div class="page">
            <div class="content">
                @yield('content')
            </div>
        </div>
    </body>
</html>