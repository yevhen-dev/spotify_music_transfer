@extends('welcome')

@section('content')
<div class="err-holder">    
    <h1>OOps..! Some problems with Spotify occurred, please, try later</h1>
    <a href="/">Go to main</a>
</div>
@stop