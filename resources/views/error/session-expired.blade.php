@extends('welcome')

@section('content')
<div class="err-holder">    
    <h1>OOps..! Session seems to be expired..</h1>
    <a href="/">Go to main</a>
</div>
@stop