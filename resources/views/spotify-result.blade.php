@extends('welcome')

@section('content')


<div class="spotify-success-wrap">
    <div class="spotify-success-holder">        
        <p>
            Your tracks were imported successfully!<br />
            Please, check your <a href="https://www.spotify.com" class="link">Spotify music</a> ,
        <p>
        <span>or, go to <a href="/" class="link">Main</a></span>
    </div>
</div>


@stop