@extends('welcome')

@section('content')
<div class="tracks-finding-zone">
    <div class="tracks-col nf-col">
        <h3>Not found:</h3>
        <div class="not-found-container">
            
        </div>
    </div><!----><div class="tracks-col vk-col">
        <h3>Vkontakte audios:</h3>
        <div class="vk-tracks-container">
            @foreach ($user_audio_data as $key => $track)
                <div class="vk-track" id="vk_track_{{ $key }}">
                    <span class="track-artist">{{ $track['artist'] }} -</span>
                    <span class="track-title">{{ $track['title'] }}</span>
                </div>
            @endforeach
            <div class="success-msg">
                <p>Great! Now, we can transfer founded tracks to Your Spotify.</p>
                <span class="percentage-acc">Searching accuracy percentage is: <b id="percent"></b><b>%</b></span>
                <a href="{{ $spotify_redir_url }}">Import to Spotify</a>
            </div>
        </div>
        <button id="search_in_spotify">Search tracks in Spotify</button>
        <div class="progress-wrap">            
            <div class="in-progress">
                <span>In progress</span>
                <span class="dot1"></span>
                <span class="dot2"></span>
                <span class="dot3"></span>
            </div>
        </div>
    </div><!----><div class="tracks-col spotify-col">
        <h3>Spotify:</h3>
        <div class="spotify-tracks-container">
            
        </div>
    </div>

</div>

<script type="text/javascript">
    
    
    // move tack to Spotify
    function moveSpotify(track)
    {
        var moved = track.clone().attr("id",track.attr('id')+'_sp');
        track.animate({ "left": "+=120%" }, "fast" ).promise().done(function () {
            track.hide('fast').promise().done(function () {
                track.remove();
            });
        });

        if($('.spotify-tracks-container .mCustomScrollBox .mCSB_container').length > 0)
        {
            $('.spotify-tracks-container .mCustomScrollBox .mCSB_container').append(moved);
        }
        else
        {
            $('.spotify-tracks-container').append(moved);
        }
        checkEnd();
    }
    // move tack to not found
    function moveNotFound(track)
    {
        var moved = track.clone().attr("id",track.attr('id')+'_nf');
        track.animate({ "left": "-=120%" }, "fast" ).promise().done(function () {
            track.hide('fast').promise().done(function () {
                track.remove();
            });
        });
        
        if($('.not-found-container .mCustomScrollBox .mCSB_container').length > 0)
        {
            $('.not-found-container .mCustomScrollBox .mCSB_container').append(moved);
        }
        else
        {
            $('.not-found-container').append(moved);
        }
        checkEnd();
    }
    
    //check if ends
    function checkEnd()
    {
        if($('.vk-tracks-container .mCustomScrollBox .mCSB_container').children('.vk-track').length == 1 || $('.vk-tracks-container .mCustomScrollBox .mCSB_container').children('.vk-track').length == 0)
        {
            $('.success-msg').show('slow');
            $('.in-progress').hide();
            
        var sp_found_count = 0;
        var not_found_count = 0;
        if($('.spotify-tracks-container .mCustomScrollBox .mCSB_container').length > 0)
        {
            sp_found_count = $('.spotify-tracks-container .mCustomScrollBox .mCSB_container .vk-track').length;
        }
        else
        {
            sp_found_count = $('.spotify-tracks-container .vk-track').length;
        }
        
        found_percent = sp_found_count / tracks_count * 100;
        
        
        $("#percent").text(Math.round(found_percent * 100) / 100);
        
        }
    }
    
    
    dotsFlag = 0;
    // main function
    function searchSpotify()
    {
        $('#search_in_spotify').hide();
        $('.in-progress').css('display','inline-block');
        if($('.vk-track').length > 0)
        {                
            $('.vk-track').each(function(i){
                var audio_id = this.id.split("_");
                var el = $(this);

                $.ajax({
                        url: "/search-audio",
                        cache: false,
                        type: "GET",
                        data: {audio_id : audio_id[2]}
                    }).done(function (res) {
                        if(res == '1')
                        {
                             moveSpotify(el);
                        }
                        else
                        {
                            moveNotFound(el);
                        }
                        dotsToggle();
                    });
            });
        }
    }
    
    function dotsToggle()
    {
        if(dotsFlag == 1)
        {
            $('.dot1').toggleClass('dot-active');
        }
        else if(dotsFlag == 2)
        {
            $('.dot2').toggleClass('dot-active');
        }
        else if(dotsFlag == 3)
        {
            $('.dot3').toggleClass('dot-active');
        }
        if(dotsFlag == 3)
        {
            dotsFlag = 0;
        }
        dotsFlag++;
    }
    
    
    // start searching
    $(document).ready(function(){
        $(".vk-tracks-container,.spotify-tracks-container, .not-found-container").mCustomScrollbar();
        
        $('.in-progress').hide();
        $('#search_in_spotify').click(function(){
            searchSpotify();
        });
        $('body').mousemove(function(){
            checkEnd();
        });
        
        tracks_count = $('.vk-tracks-container .mCustomScrollBox .mCSB_container .vk-track').length;
    });
    
    
</script>
    
    
@stop