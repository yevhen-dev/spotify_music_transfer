<?php

namespace App\Http\Controllers;

class VkController extends Controller
{
    private $vk_redir_url = 'http://localhost/vk-auth';
    private $vk_api_auth_endpoint = 'http://oauth.vk.com/authorize';
    private $vk_api_token_url = 'https://oauth.vk.com/access_token';
    
    public function __construct()
    {
        session_start();
    }
    
    public function welcome()
    { 
        session_unset();
        // set params for vk auth
        $params = array(
            'client_id'     => $_ENV['VK_APP_CLIENT_ID'],
            'redirect_uri'  => $this->vk_redir_url,
            'scope' => 'audio',
            'response_type' => 'code'
        );

        $redir_url = $this->vk_api_auth_endpoint . '?' . urldecode(http_build_query($params));
        
        return view('index', compact('redir_url'));
    }
    
    
    public function vkAuth()
    {
        // get code in $_GET
        if (isset($_GET['code']))
        {
            $code = $_GET['code'];
            $params = array(
                'client_id' => $_ENV['VK_APP_CLIENT_ID'],
                'client_secret' => $_ENV['VK_ACCESS_TOKEN'],
                'code' => $_GET['code'],
                'scope' => 'audio',
                'redirect_uri' => $this->vk_redir_url
            );
            
            // get vk-api access_token
            $api_token = json_decode(file_get_contents($this->vk_api_token_url . '?' . urldecode(http_build_query($params))), true);
            
            if (isset($api_token['access_token'])) 
            {
                $params = array(
                    'access_token' => $api_token['access_token'],
                    'owner_id'         => $api_token['user_id'],
                    'count'   => 3000,
                );
                
                try 
                {
                    $userInfo = json_decode(file_get_contents('https://api.vk.com/method/audio.get' . '?' . urldecode(http_build_query($params))), true);
                    if(!empty($userInfo['response']) && is_array($userInfo['response']))
                    {
                        array_shift($userInfo['response']);
                        $_SESSION['user_audio_data'] = $userInfo['response'];
                    }
                } 
                catch (Exception $exc) 
                {
                    echo $exc->getTraceAsString();
                }
                
                return redirect('vk-audio-result');
            }
        }
    }
    
    
    public function ajax_spotifySearching()
    {
        if(array_key_exists('audio_id', $_GET) && !empty($_SESSION['user_audio_data']))
        {
            $audio_id  = intval($_GET['audio_id']);
            $currentTrack = $_SESSION['user_audio_data'][$audio_id];
            
            $foundedTracks = array();
            $notFounded = array();
            
            $trackName = urlencode(trim($currentTrack['artist']).' '.trim($currentTrack['title']));
            $trackNameClear = trim($currentTrack['artist']).' - '.trim($currentTrack['title']);
            $clearTitle = trim($currentTrack['title']);
            $clearArtist = trim($currentTrack['artist']);
            
            $response = json_decode(file_get_contents($this->spotify_search_song_endpoint.$trackName.'&type=track&limit=1',true));
            if(!empty($response->tracks->items[0]->uri))
            {
                //done
                $founded = array();
                $founded['track_name'] = $trackNameClear;  
                $founded['track_uri'] = $response->tracks->items[0]->uri;
                $_SESSION['founded_tracks'][] = $founded;
                
                return '1';
            }
            else // try to found by title only
            {       
                $response_title = json_decode(file_get_contents($this->spotify_search_song_endpoint. urlencode($clearTitle).'&type=track',true));
                if(isset($response_title->tracks->items[0]->artists[0]->name))
                {   
                    foreach ($response_title->tracks->items as $item)
                    {
                        $exploded = explode(' ', $clearArtist);
                        $expr = '/'.$exploded[0].'/i';
                        $notFoundFlag = 0;
                        if (preg_match($expr, $item->artists[0]->name))
                        {
                            // done
                            $founded = array();
                            $founded['track_name'] = $trackNameClear;
                            $founded['track_uri'] = $item->uri;
                            $_SESSION['founded_tracks'][] = $founded;
                            
                            return '1';
                        }
                    }
                     // add track to not founded =(
                     if($notFoundFlag >= 1) { $_SESSION['not_founded_tracks'][] = $trackNameClear; return '0'; }
                }
                else
                {
                    $_SESSION['not_founded_tracks'][] = $trackNameClear;
                    return '0';
                }
            }
        }
        
        return '0';
        
    }
    
    public function vkAudioResult()
    {
        // return data from SESSION
        if(!empty($_SESSION['user_audio_data']))
        {
            $user_audio_data = $_SESSION['user_audio_data'];
            
            $params = array(
                'client_id'     => $_ENV['SPOTIFY_APP_CLIENT_ID'],
                'redirect_uri'  => $this->spotify_redirect,
                'scope' => 'user-read-private playlist-read-private playlist-modify-private playlist-modify-public',
                'response_type' => 'code'
            );

            $spotify_redir_url = $this->spotify_auth_url . '?' . http_build_query($params);
            
            return view('vk-audio-result', compact('user_audio_data', 'spotify_redir_url'));
        }
        else
        {
            return view('error.session-expired');
        }

    }
    
    
}
