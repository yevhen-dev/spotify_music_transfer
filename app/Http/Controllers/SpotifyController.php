<?php

namespace App\Http\Controllers;

class SpotifyController extends Controller
{
    //spotify endpoints and links
    protected $spotify_auth_url = 'https://accounts.spotify.com/authorize';
    protected $spotify_redirect = 'http://localhost/spotify-auth';
    protected $spotify_api_token_endpoint = 'https://accounts.spotify.com/api/token';
    protected $spotify_search_song_endpoint = 'https://api.spotify.com/v1/search?q=';
    
    function __construct() 
    {
        session_start();
    }
    
    public function spotifyImport()
    {
        if (isset($_GET['code']))
        {
            $data = array
            (
                'grant_type' => 'authorization_code',
                'code' => $_GET['code'],
                'client_id' => $_ENV['SPOTIFY_APP_CLIENT_ID'],
                'client_secret' => $_ENV['SPOTIFY_ACCESS_TOKEN'],
                'redirect_uri' => $this->spotify_redirect
            );
            $options = array(
                'http' => array(
                    'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                    'method'  => 'POST',
                    'content' => http_build_query($data)    
                )
            );
            $context = stream_context_create($options);
            $result = file_get_contents($this->spotify_api_token_endpoint, false, $context);
            
            if($result != false)
            {
                $result = json_decode($result);
                
                $url_me = 'https://api.spotify.com/v1/me';
                $header = "Authorization: Bearer ".$result->access_token;
                $opts = array(
                  'http'=>array(
                    'method'=>"GET",
                    'header'=>$header
                  )
                );

                $context = stream_context_create($opts);
                // Geting User info
                $user = json_decode(file_get_contents($url_me, false, $context));
                
                //Getting user playlists
                if (!empty($user->id))
                {
                    $playlist_url = 'https://api.spotify.com/v1/me/playlists';
                    
                    $header = "Authorization: Bearer ".$result->access_token;
                    $opts = array(
                      'http'=>array(
                        'method'=>"GET",
                        'header'=>$header
                      )
                    );
                    
                    $context = stream_context_create($opts);
                    // Open the file using the HTTP headers set above
                    $playlists = json_decode(file_get_contents($playlist_url, false, $context));
                    
                    // Create a playlist
                    $create_list_url = 'https://api.spotify.com/v1/users/'.$user->id.'/playlists';
                    $data = array
                    (
                        'name' => 'Vk-music('.date('d.m.Y', time()).')',
                    );
                    $options = array(
                        'http' => array(
                            'header'  => "Authorization: Bearer ".$result->access_token."\r\n" .
                                                "Content-Type: application/json\r\n",
                            'method'  => 'POST',
                            'content' => json_encode($data)
                        )
                    );
                    
                    $context = stream_context_create($options);
                    $playlist = file_get_contents($create_list_url, false, $context);
                    
                    $playlist = json_decode($playlist);
                    
                    // add founded tracks to playlist
                    if(isset($playlist->id))
                    {
                        $add_tracks_url = 'https://api.spotify.com/v1/users/'.$user->id.'/playlists/'.$playlist->id.'/tracks';
                        if(!empty($_SESSION['founded_tracks']))
                        {
                            foreach ($_SESSION['founded_tracks'] as $track)
                            {
                                $uris['uris'][] = $track['track_uri'];
                            }
                        }
                        else
                        {
                            return view('error.session-expired');
                        }
                       
                        $options = array(
                            'http' => array(
                                'header'  => "Authorization: Bearer ".$result->access_token."\r\n" .
                                                    "Content-Type: application/json\r\n",
                                'method'  => 'POST',
                                'content' => json_encode($uris)
                            )
                        );
                        $context = stream_context_create($options);
                        $playlist_answer = file_get_contents($add_tracks_url, false, $context);
                        
                        return redirect('spotify-result');
                    }
                }  
            }
        }
        
        return view('error.spotify-err');
    }
    
    
    public function spotifyResult()
    {
        session_destroy();
        
        return view('spotify-result');
    }
    
    
    
}

