<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    //spotify endpoints and links
    protected $spotify_auth_url = 'https://accounts.spotify.com/authorize';
    protected $spotify_redirect = 'http://localhost/spotify-auth';
    protected $spotify_api_token_endpoint = 'https://accounts.spotify.com/api/token';
    protected $spotify_search_song_endpoint = 'https://api.spotify.com/v1/search?q=';
    
    
}
