<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/



$app->group(['namespace' => 'App\Http\Controllers'], function($group){
    $group->get('/', 'VkController@welcome');
    $group->get('vk-auth', 'VkController@vkAuth');
    $group->get('vk-audio-result', 'VkController@vkAudioResult');
    $group->get('search-audio', 'VkController@ajax_spotifySearching');
    
    $group->get('spotify-auth', 'SpotifyController@spotifyImport');
    $group->get('spotify-result', 'SpotifyController@spotifyResult');
});
